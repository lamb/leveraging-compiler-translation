# leveraging-compiler-translation 

Repository used for maintaining evaluation of OpenACC+OpenARC across AMD, and Nvidia platforms.

Created for P3HPC '22 submission.


## Systems

<ol>
<li> OACCIS Gilgamesh - AMD Instinct GPU, AMD EPYC CPUs
<li> OACCIS Gilgamesh - Nvidia A100 GPU
<ol>
