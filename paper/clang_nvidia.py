import pandas as pd
import seaborn as sns
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

cols_data = ["Device", "Backend", "Compiler", "Benchmark", "Mapping", "Runtime (s)"]

# Latex fonts
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
sns.set_style("darkgrid", {"font.family": ['serif'], "font.serif": ['Computer Modern']})
#sns.set_context("paper", 1, {"font.size":10,"axes.titlesize":10,"axes.labelsize":10}) 
#sns.set_palette("Set2")

files = ["data/MI100.csv", "data/A100.csv", "data/Xe.csv"]

data = pd.concat((pd.read_csv(f, usecols=cols_data) for f in files))

# Shorten Benchmark Names
data['Benchmark'] = data['Benchmark'].str.slice(0,7)

# Filter out A100
data = data[(data["Device"] == "A100")]

# Filter out OpenMP/OpenCL
data1 = data[(data["Backend"] == "OpenCL")]
data2 = data[(data["Backend"] == "OpenMP")]


max_height = 150

device_order=['MI100','A100','Xe']
color_order=['orange','green','blue']
backend_order=['CUDA','HIP','OpenCL','OpenMP','OpenACC']

fig, (ax1, ax2) = plt.subplots(nrows=2)

sns.barplot(data=data1, x="Benchmark", y="Runtime (s)", 
            hue="Compiler", #dodge=False,
		    estimator=min, ci=None, ax=ax1, palette=["C0", "C2"])

sns.barplot(data=data2, x="Benchmark", y="Runtime (s)", 
            hue="Compiler", #dodge=False,
		    estimator=min, ci=None, ax=ax2, palette=["C0", "C1"])

#g.set_axis_labels("", "Runtime(s)")
#g.set(ylim=(0, max_height))
#g.set_titles("{col_name}")
ax1.set_ylim(0, max_height)
ax1.set_title("OpenCL")
ax1.set_xlabel("")
ax2.set_xlabel("")

ax2.set_ylim(0, max_height)
ax2.set_title("OpenMP")

#plt.legend(loc='center right')



# Set manual label for bars outside range
for p in ax2.patches:
  if p.get_height() > max_height:
    ax2.annotate("%.2f" % p.get_height(), 
              (p.get_x() + p.get_width() / 2. + 0.4, max_height * 0.8),
              ha='center', va='top', fontsize=8, color='black', rotation=90, 
              xytext=(0, 20), textcoords='offset points')  

#plt.savefig('tmp.pdf', dpi=1000)
plt.tight_layout()
plt.show()
