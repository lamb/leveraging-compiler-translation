import pandas as pd
import seaborn as sns
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

cols_data = ["Device", "Backend", "Compiler", "Benchmark", "Mapping", "Runtime (s)"]

# Latex fonts
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
sns.set_style("darkgrid", {"font.family": ['serif'], "font.serif": ['Computer Modern']})

files = ["data/MI100.csv", "data/A100.csv"]

data = pd.concat((pd.read_csv(f, usecols=cols_data) for f in files))

# Shorten Benchmark Names
data['Benchmark'] = data['Benchmark'].str.slice(0,7)

# Filter out nvc+openmp
data = data[(data["Backend"] != "OpenMP") | (data["Compiler"] != "nvc")]

# Filter out clang+opencl
data = data[(data["Backend"] != "OpenCL") | (data["Compiler"] != "clang")]

# Normalize by FLOPs 
a100_flops = 19.5
MI100_flops = 13.3

data["Runtime (s)"] = np.where(data["Device"] == 'A100', a100_flops * data["Runtime (s)"], data["Runtime (s)"])
data["Runtime (s)"] = np.where(data["Device"] == 'MI100', MI100_flops * data["Runtime (s)"], data["Runtime (s)"])

plt.rc('xtick', labelsize=9)
max_height = 2300

device_order=['MI100','A100']
color_order=['orange','green']
backend_order=['CUDA','HIP','OpenCL','OpenMP','OpenACC']

g = sns.catplot(data=data, kind="bar", x="Benchmark", y="Runtime (s)", 
                  hue="Device", col="Backend", legend=False,
		  estimator=min, ci=None, hue_order=device_order, col_wrap=3,
		  col_order=backend_order, palette=["C1", "C2", "C0"])

g.set_axis_labels("", "")
g.set(ylim=(0, max_height))
g.set_titles("{col_name}")

plt.legend(loc='center right')
plt.text(y=1100, x=-8, rotation='vertical', s="Runtime(s) x TFLOPS(single-precision)")


# Set manual label for bars outside range
axes=g.axes
for i in [2,3]:
  ax = axes[i]
  for p in ax.patches:
    if p.get_height() > max_height:
     ax.annotate("%.2f" % p.get_height(), 
               (p.get_x() + p.get_width() / 2. + 0.3, max_height * 0.8),
               ha='center', va='top', fontsize=8, color='black', rotation=90, 
               xytext=(0, 20), textcoords='offset points')  

plt.show()
