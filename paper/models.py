import pandas as pd
import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt

cols_data = ["Device", "Backend", "Compiler", "Benchmark", "Mapping", "Runtime (s)"]

# Latex fonts
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
sns.set_style("darkgrid", {"font.family": ['serif'], "font.serif": ['Computer Modern']})

files = ["data/MI100.csv", "data/A100.csv"]

data = pd.concat((pd.read_csv(f, usecols=cols_data) for f in files))

# Shorten Benchmark Names
data['Benchmark'] = data['Benchmark'].str.slice(0,7)

# Filter out nvc+openmp
data = data[(data["Backend"] != "OpenMP") | (data["Compiler"] != "nvc")]

# Filter out clang+opencl
data = data[(data["Backend"] != "OpenCL") | (data["Compiler"] != "clang")]

plt.rc('xtick', labelsize=9)
max_height = 150

#pd.set_option('display.max_rows', 1000)
#print(data)

#sns.set_palette("cubehelix", 5)
backend_order=['CUDA','HIP','OpenCL','OpenMP','OpenACC']

g = sns.catplot(data=data, kind="bar", x="Benchmark", y="Runtime (s)", 
                  hue="Backend", row="Device", legend=False,
		  estimator=min, ci=None, hue_order=backend_order,
		  palette=["C2", "C1", "C0", "C3", "C4"])

g.set_axis_labels("", "Runtime(s)")
g.set(ylim=(0, max_height))
g.set_titles("{row_name}")

plt.legend(loc='upper right')



# Set manual label for bars outside range
axes=g.axes
for i in [0,1]:
  for j in [0]:
    ax = axes[i][j]
    for p in ax.patches:
      if p.get_height() > max_height:
       ax.annotate("%.2f" % p.get_height(), 
                 (p.get_x() + p.get_width() / 2. + 0.2, max_height * 0.8),
                 ha='center', va='top', fontsize=8, color='black', rotation=90, 
                 xytext=(0, 20), textcoords='offset points')  

plt.show()
