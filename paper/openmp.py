import pandas as pd
import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt

cols_data = ["Device", "Backend", "Compiler", "Benchmark", "Mapping", "Runtime (s)"]

# Latex fonts
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
sns.set_style("darkgrid", {"font.family": ['serif'], "font.serif": ['Computer Modern']})

files = ["data/A100.csv", "data/MI100.csv"]

data = pd.concat((pd.read_csv(f, usecols=cols_data) for f in files))


# Shorten Benchmark Names
data['Benchmark'] = data['Benchmark'].str.slice(0,7)

# Filter OpenMP
data = data[data["Backend"] == "OpenMP"]
data = data[data["Compiler"] != "nvc"]

plt.rc('xtick', labelsize=9)
max_height = 900

#print(data)

#sns.set_palette("cubehelix", 5)

g = sns.catplot(data=data, kind="bar", x="Benchmark", y="Runtime (s)", 
                  estimator=min, ci=None, hue="Mapping", row="Device", legend=False,
		  palette=["C1", "C2", "C0"]
		)

g.set_axis_labels("", "Runtime(s)")
g.set(ylim=(0, max_height))
g.set_titles("{row_name}")

leg_text = ["literal", "gpu-friendly", "cpu-friendly"]
plt.legend(leg_text, loc='center right')
plt.legend(leg_text, bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

# Set manual label for bars outside range
axes=g.axes #annotate axis = seaborn axis
for i in [0,1]:
  for j in [0]:
    ax = axes[i][j]
    for p in ax.patches:
      if p.get_height() > max_height:
        ax.annotate("%.2f" % p.get_height(), 
                 (p.get_x() + p.get_width() / 2., max_height * 0.8),
                 ha='center', va='top', fontsize=8, color='white', rotation=90, 
                 xytext=(0, 20), textcoords='offset points')  

plt.show()
