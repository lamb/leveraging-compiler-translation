#include <stdio.h>
#include <stdlib.h>
//#include <assert.h>

float myadd(float a, float b) {
  return a + b;
}

int main(int argc, char** argv) {
  int workers, gangs, size;
  float *A, *B, *C;
  int i;
  int error = 0;

  workers = 10000;
  gangs = 10000;
  size = workers * gangs;

  A = (float*) malloc(size * sizeof(float));
  B = (float*) malloc(size * sizeof(float));
  C = (float*) malloc(size * sizeof(float));

  for (i = 0; i < size; i++) {
    A[i] = (float) i;
    B[i] = (float) i * 100;
  }

  //printf("omp_get_num_devices(): %d\n", omp_get_num_devices());
  //printf("omp_get_default_device(): %d\n", omp_get_default_device());

  #pragma acc data copyin(A[0:size], B[0:size]) copyout(C[0:size]) 
  { 
    #pragma acc parallel loop gang present(A[0:size], B[0:size], C[0:size])
    for (int i = 0; i < size; i++) {  
      for (int j = 0; j < size; ++j) {
        //C[i] = A[i] + B[i] + myadd(i,j);
        C[i] = A[i] + B[i];

      }
    }
  }


  //for (i = 0; i < size; i++) {
  //  if (C[i] != (float) i + (float) i * 100 + myadd(size-1, i) ) error++;
  //}

  //printf("workers:%d, gangs:%d, size:%d, error:%d\n", workers, gangs, size, error);

  return 0;
}
