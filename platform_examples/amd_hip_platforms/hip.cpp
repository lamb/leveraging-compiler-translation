#include <hip/hip_runtime.h>
int main() {

  hipError_t err;
  hipDevice_t hipDevice;

  err = hipInit(0);
  if (err != hipSuccess) printf("[%s:%d][%s] error[%d]\n", __FILE__, __LINE__, __func__, err);
  hipDeviceGet(&hipDevice, 0);

  char name[256];
  hipDeviceGetName(name, sizeof(name), hipDevice);
  printf("%s\n", name);

  const char* platform = std::getenv("HIP_PLATFORM");
  if (platform != NULL && !strcmp(platform, "nvcc")) {
    printf("%s\n", platform);
  } else{
    printf("nope\n");
  }

  hipCtx_t hipContext;
  err = hipCtxCreate(&hipContext, 0, hipDevice);
  if (err != hipSuccess) printf("[%s:%d][%s] error[%d]\n", __FILE__, __LINE__, __func__, err);

  fprintf(stderr, "[DEBUG] A HIP context is created or loaded.\n");


  hipModule_t hipModule;

  err = hipModuleLoad(&hipModule, "openarc_kernel.hip");
  if (err != hipSuccess) printf("[%s:%d][%s] error[%d]\n", __FILE__, __LINE__, __func__, err);

  printf("%s\n", hipGetErrorName(err));
}
