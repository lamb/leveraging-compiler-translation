#include <stdio.h>
#include <stdlib.h>

#define N 100

double myadd (int a, int b) { return a + b; }
#pragma omp declare target to(myadd)

int main() {

  double a[N], b[N], c[N];

  for (int i = 0; i < N; ++i) {
    a[i] = i;
    b[i] = 2*i;
  }

  printf("Starting loop\n");
  #pragma omp target teams distribute parallel for map(to:a[0:N], b[0:N]) map(from:c[0:N])
  for (int i = 0; i < N; ++i) {
    c[i] = myadd(a[i], b[i]);
  }
  printf("Ending loop\n");

  for (int i=0; i < 10; ++i) {
    if (c[i] != myadd(i + i, i) ) {
      printf("Verification Failed!\n");
      exit(0);
    }
  }

  printf("Verification Succesful!\n");

  return 0;
}
