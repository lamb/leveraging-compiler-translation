# 303.ostencil

Simple stencil kernel (3-d loop)


## Example Build

<ol>
<li> Build OpenARC

        $ cd openarc/
        $ export OPENARC_ARCH=0
        $ make

<li> Run OpenARC

        $ cd amd-intel-nvidia-eval
        $ vim java.sh // modify to select CUDA
        $ bash java.sh // soft link of ../java.sh

<li> Build executable

        $ make backend=CUDA (or backend=OpenACC)

<li> Test executable

        $ make run_test

<li> Time executable

        $ make run_ref

<li> Tau execution

        $ vim Makefile // modifiy TAU flags
        $ make run_tau

<ol>
  

