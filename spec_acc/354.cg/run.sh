#!/bin/bash

ver=0

rm results.txt

for ver in 0 1 2 3 4
do
  make purge;
  make VER=$ver;
  printf "Version $ver\n" >> results.txt
  { time ./cg ; }  &>> results.txt
  printf "\n" >> results.txt

  ((ver++))
done
