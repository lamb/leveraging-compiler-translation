#ifndef __TYPE_H__
#define __TYPE_H__

typedef enum { _false, _true } logical;

typedef struct { 
  double real;
  double imag;
} dcomplex;


#define min_t(x,y)    ((x) < (y) ? (x) : (y))
#define max_t(x,y)    ((x) > (y) ? (x) : (y))

#endif //__TYPE_H__
