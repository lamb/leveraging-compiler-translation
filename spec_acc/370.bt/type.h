#ifndef __TYPE_H__
#define __TYPE_H__

typedef enum { _false, _true } logical;
typedef struct { 
  double real;
  double imag;
} dcomplex;


#define _min(x,y)    ((x) < (y) ? (x) : (y))
#define _max(x,y)    ((x) > (y) ? (x) : (y))

#endif //__TYPE_H__
