# SPEC Accel Benchmarks

Directory containg scripts and source code for Spec Accel Benchmark evaluations.


## Directories

<ol>
<li> 303, 314 - single precisions benchmarks 
<li> 352, 354, 357, 370 - double precision benchmarks
<li> makefiles - platform specific makefiles loaded by scripts
<ol>

## Scripts

<ol>
<li> eval.py - python script for running multiple configurations at once (python eval.py --help)
<li> eval.sh - used internally by eval.py
<li> hipify.sh - used internally by eval.py
<li> java.sh - used to manually run OpenARC in subdirectories (soft-linked)
<li> openarcArgs.txt - common arguments loaded by eval.sh
<li> scrub.sh - clean up (bash scrub.sh)
<ol>


