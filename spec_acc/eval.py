#!/usr/bin/python
import os
import subprocess
from time import gmtime, strftime
import sys
import argparse
import socket

host = socket.gethostname()

# Platforms

class Platform:
  def __init__ (self, host, device, backend, compiler):
    self.host = host 
    self.device = device
    self.backend = backend
    self.compiler = compiler

  def __str__(self):
    return self.host + "," + self.device + "," + self.backend + "," + self.compiler

platforms = []

#platforms.append(Platform("gilgamesh", "A100", "HIP", "hipcc"))
#platforms.append(Platform("gilgamesh", "A100", "CUDA", "nvcc"))
#platforms.append(Platform("gilgamesh", "A100", "CUDA", "nvcc"))
#platforms.append(Platform("gilgamesh", "A100", "OpenCL", "clang"))
#platforms.append(Platform("gilgamesh", "A100", "OpenCL", "nvcc"))
#platforms.append(Platform("gilgamesh", "A100", "OpenMP", "clang")) 
#platforms.append(Platform("gilgamesh", "A100", "OpenMP", "nvc")) 
#platforms.append(Platform("gilgamesh", "A100", "OpenACC", "nvc")) 

#platforms.append(Platform("cousteau", "MI100", "HIP", "hipcc"))
platforms.append(Platform("cousteau", "MI100", "OpenMP", "hipcc"))
#platforms.append(Platform("cousteau", "MI100", "OpenCL", "hipcc"))
#platforms.append(Platform("cousteau", "MI100", "CUDA", "hipcc"))

#platforms.append(Platform("Explorer", "EPYC", "OpenMP", "hipcc"))

#platforms.append(Platform("godzilla", "Xe", "OpenMP", "icx"))
#platforms.append(Platform("godzilla", "Xe", "OpenCL", "icpx"))	

	
result_dir = "/home/users/jlambert/amd-intel-nvidia-eval/spec_acc"

def try_print (s) :
  try:
    print(s)
  except:
    pass

# Parse Input
parser = argparse.ArgumentParser(description='Script to compile, run, and collect information from OpenACC programs, using OpenARC and a variety of different backends', epilog="Result files are written to " + result_dir)
parser.add_argument('-b','--benchmarks', nargs='+', help='List benchmarks to be evaluated', required=True)
parser.add_argument('-m','--mappings', nargs='+', help='List mappings to be evaluated', required=True)
args = parser.parse_args()

subprocess.check_output(["rm", "-f", "compiler_out.txt"])

# Iterate over platforms
for platform in platforms:

  print("-------------------")
  print(platform)

  for benchmark in args.benchmarks:
    for mapping in args.mappings:
      print(benchmark + ", mapping:" + mapping)

      subprocess.check_output(["bash", "eval.sh", 
                                    platform.host,
                                    platform.device,
                                    platform.backend,
                                    platform.compiler,
                                    benchmark,
                                    mapping])

      #print("  " + subprocess.check_output(["cat", benchmark + "/time.txt"]) + "s")
