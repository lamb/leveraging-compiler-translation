#!/bin/bash

working_directory=$PWD

# Platform Details
host=$1
device=$2
backend=$3
compiler=$4
benchmark=$5
mapping=$6

cd $benchmark

# Build
echo "$host,$device,OpenACC,$compiler,$benchmark" >> $working_directory/compiler_out.txt

make scrub >> $working_directory/compiler_out.txt

verLevel=0
openarcrt="${openarc}/openarcrt"
openarclib="${openarc}/lib"

if [[ $mapping == 0 ]]
then
adv=0
dev=0
fi

if [[ $mapping == 1 ]]
then
adv=1
dev=0
fi

if [[ $mapping == 2 ]]
then
adv=1
dev=1
echo "mapping: $mapping"
fi

ulimit -s unlimited

if [[ $backend == "OpenMP" ]]
then
java -classpath $openarclib/cetus.jar:$openarclib/antlr.jar openacc.exec.ACC2GPUDriver \
    -verbosity=${verLevel} \
    -showInternalAnnotations=0 \
    -SkipGPUTranslation=1 \
    -ompaccInter=4 \
    -parallelismMappingStrat=enableAdvancedMapping=$adv:vectorize=1:vectorfriendlyanalysis=2:devicetype=$dev \
    -gpuConfFile=../openarcArgs.txt \
    *.c \
    >> $working_directory/compiler_out.txt
fi

if [[ $backend == "OpenACC" ]]
then
java -classpath $openarclib/cetus.jar:$openarclib/antlr.jar openacc.exec.ACC2GPUDriver \
    -verbosity=${verLevel} \
    -showInternalAnnotations=1 \
    -SkipGPUTranslation=1 \
    -gpuConfFile=../openarcArgs.txt \
    -parallelismMappingStrat=enableAdvancedMapping=$adv:vectorize=1:vectorfriendlyanalysis=2:devicetype=$dev \
    *.c \
    >> $working_directory/compiler_out.txt
fi

if [[ $backend == "CUDA" || $backend == "OpenCL" || $backend == "HIP" ]]
then
java -classpath $openarclib/cetus.jar:$openarclib/antlr.jar openacc.exec.ACC2GPUDriver \
    -verbosity=${verLevel} \
    -showInternalAnnotations=0 \
    -parallelismMappingStrat=enableAdvancedMapping=$adv:vectorize=1:vectorfriendlyanalysis=2:devicetype=$dev \
    -gpuConfFile=../openarcArgs.txt \
    *.c \
    >> $working_directory/compiler_out.txt
fi

# hipify
#source ../hipify.sh
#echo "Running hipify-perl"

echo "make host=${host} backend=${backend} device=${device};" >> $working_directory/compiler_out.txt
make host=${host} backend=${backend} device=${device} &>> $working_directory/compiler_out.txt

# Run
make run_ref >> $working_directory/compiler_out.txt

# Record
runtime=`cat time.txt`

echo "$host,$device,OpenACC,$backend,$compiler,$benchmark,$mapping,$runtime" >> $working_directory/results.txt

cd $working_directory
