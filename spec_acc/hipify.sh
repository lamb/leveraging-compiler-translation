shopt -s nullglob

cd cetus_output;
for f in *.cpp; do
  hipify-perl $f > $f.tmp;
  mv $f.tmp $f
done

hipify-perl openarc_kernel.cu > openarc_kernel.hip.cpp
rm openarc_kernel.cu
cd ..;
