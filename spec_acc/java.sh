verLevel=0
openarcrt="${openarc}/openarcrt"
openarclib="${openarc}/lib"

# OpenACC
#java -classpath $openarclib/cetus.jar:$openarclib/antlr.jar openacc.exec.ACC2GPUDriver \
#    -addIncludePath=$openarcrt \
#    -verbosity=$verLevel \
#    -showInternalAnnotations=1 \
#    -SkipGPUTranslation=1 \
#    -gpuConfFile=../openarcArgs.txt \
#    -parallelismMappingStrat=enableAdvancedMapping=$adv:vectorize=1:vectorfriendlyanalysis=2:devicetype=$dev \
#    *.c 

# OpenMP
adv=0
dev=0
# 0:0 for mapping 1
# 1:0 for mapping 2
# 1:1 for mapping 3

#java -classpath $openarclib/cetus.jar:$openarclib/antlr.jar openacc.exec.ACC2GPUDriver \
#    -addIncludePath=$openarcrt \
#    -verbosity=$verLevel \
#    -showInternalAnnotations=0 \
#    -SkipGPUTranslation=1 \
#    -ompaccInter=4 \
#    -parallelismMappingStrat=enableAdvancedMapping=$adv:vectorize=1:vectorfriendlyanalysis=2:devicetype=$dev \
#    -gpuConfFile=../openarcArgs.txt \
#    *.c

# CUDA / OpenCL / HIP
targetArch=1
#        =0 for CUDA
#        =1 for general OpenCL
#        =5 for AMD HIP

java -classpath $openarclib/cetus.jar:$openarclib/antlr.jar openacc.exec.ACC2GPUDriver \
    -targetArch=$targetArch \
    -addIncludePath=$openarcrt \
    -verbosity=$verLevel \
    -showInternalAnnotations=0 \
    -parallelismMappingStrat=enableAdvancedMapping=$adv:vectorize=1:vectorfriendlyanalysis=2:devicetype=$dev \
    -gpuConfFile=../openarcArgs.txt \
    *.c
