include ${openarc}/make.header

########################################
# Set the input C source files (CSRCS) #
########################################
CSRCS = *.c
CFLAGS1 = -I. -O3
CLIBS1 =  -lm

include ${openarc}/make.template
